class FilmManipulation
{
    constructor(){}

    sendFile(e)
    {
        let file = e.target.files;
        let data = new FormData();
        data.append('files', file[0]);
        // console.log(file);
        fetch('/load_file', {
                method: 'POST',
                body: data
            })
            .then(response => {
                return response.json();
            })
            .then(e => {
                console.log(e);
            })
        ;
    }

    findFilm(event)
    {
        let select = document.getElementById('sort_key');
        let selected_val = select.options[select.selectedIndex].value;
        if (selected_val === 'None')
            return;
        let search_name = document.getElementById('search_name').value;
        // console.log(selected_val, search_name)
        let str = "val="+selected_val+"&name="+search_name;
        let propObj = {
            method: 'GET',
            headers: {
                method: 'GET',
                credentials: 'include',
            }
        };
        fetch('/find_by?'+str, propObj)
            .then(e => {
                return e.json();
            })
            .then(e => {
                document.getElementById('content_block').innerHTML = e;
                let del_buttons = document.getElementsByClassName("delete_button");
                // console.log(FilmManipulation.delFilm);
                for (let item of del_buttons) {
                    item.onclick = FilmManipulation.delFilm;
                }
            })
            .catch(e => {
                console.log(e);
            })
        ;
    }

    delFilm(event)
    {
        let cliecked = event.target.value;

        let propObj = {
            method: 'POST',
            headers: {
                method: 'POST',
                credentials: 'include',
            },
            body: JSON.stringify({
                id: cliecked
            })
        };
        fetch('del_film', propObj)
            .then(e => {
                return e.json();
            })
            .then(e => {
                document.getElementById('block_'+e.id).remove();
            })
            .catch(e => {
                console.log(e);
            })
        ;
    }

    static delFilm(event)
    {
        let cliecked = event.target.value;

        let propObj = {
            method: 'POST',
            headers: {
                method: 'POST',
                credentials: 'include',
            },
            body: JSON.stringify({
                id: cliecked
            })
        };
        fetch('del_film', propObj)
            .then(e => {
                return e.json();
            })
            .then(e => {
                document.getElementById('block_'+e.id).remove();
            })
            .catch(e => {
                console.log(e);
            })
        ;
    }

    addFilm(event)
    {
        const get_values_arr = array => {
            let res = [];
            for (let item of array) {
                res.push(item.value);
            }
            return res;
        };
        let actor_names = document.getElementsByClassName('actor_name');
        let film_name = document.getElementById('film_name').value;
        let film_year = document.getElementById('film_year').value;
        let film_format = document.getElementById('film_format').value;
        let send_obj = {
            actor_names: get_values_arr(actor_names),
            film_name: (film_name),
            film_year: (film_year),
            film_format: (film_format),
        };
        let propObj = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(send_obj)
        };
        // console.log(send_obj);
        fetch('/add_film', propObj)
            .then(e => {
                return e.json();
            }, console.error)
            .then(e => {
                let parent = document.getElementById('content_block');
                let tmp = parent.innerHTML;
                parent.innerHTML = e.new_film + tmp;

                let del_buttons = document.getElementsByClassName("delete_button");
                // console.log(FilmManipulation.delFilm);
                for (let item of del_buttons) {
                    item.onclick = FilmManipulation.delFilm;
                }
            })
            .catch(e => {
                console.log(e);
            })
        ;
    }
}