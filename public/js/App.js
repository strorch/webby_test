class App
{
    constructor()
    {
        this.filmManipulation = new FilmManipulation();
    }

    init_del_buttons()
    {
        let del_buttons = document.getElementsByClassName("delete_button");
        for (let item of del_buttons) {
            item.onclick = FilmManipulation.delFilm;
        }
    }
    init_add_film()
    {
        let add_film = document.getElementById('add_film');
        add_film.onclick = this.filmManipulation.addFilm;
    }
    init_add_actor()
    {
        let add_actor = document.getElementById('add_actor');
        add_actor.onclick = e => {
            let str =   "<div style='display: flex;flex-direction: row'>" +
                        "   <input class='actor_name' type='text' />" +
                        "   <button class='del_button_'>Del</button>" +
                        "</div>";
            let container = document.getElementById('actor_names_container');
            container.innerHTML += str;

            let elements = document.getElementsByClassName('del_button_');
            for (let el of elements) {
                el.onclick = (e) => {
                    let parent = e.target.parentNode;
                    parent.remove();
                };
            }
        };
    }
    init_find_by()
    {
        let find_butt = document.getElementById('find_by');
        find_butt.onclick = this.filmManipulation.findFilm;
    }
    init_read_text()
    {
        let find_butt = document.getElementById('file_input');
        find_butt.onchange = this.filmManipulation.sendFile;
    }

    dialog_init()
    {
        document.getElementById('show_dialog').onclick = e => {
            document.getElementById('search_block').style.display = 'block';
        };
        document.getElementById('close_window').onclick = e => {
            document.getElementById('search_block').style.display = 'none';
        };
    }

    init()
    {
        this.init_add_actor();
        this.init_add_film();
        this.init_del_buttons();
        this.init_find_by();
        this.init_read_text();
        this.dialog_init();
    }
}