<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test</title>
    <link type="text/css" rel="stylesheet" href="./public/css/front_page.css?<?php echo time(); ?>">
</head>
<body>
<div id="parent_header">
    <div id="header" style="display: block;position: absolute;left: 2vw; ">
        <a href="/" id='front_head' style="text-align: center">front page</a>
    </div>
    <div style="margin-left: 5vw;">
        <div id="header" style="width: 100%">
            Film Info
        </div>
    </div>
</div>
<div id="content_block">
    <?php
    echo $film_list;
    ?>
</div>
</body>
</html>