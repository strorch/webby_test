<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test</title>
    <link type="text/css" rel="stylesheet" href="./public/css/front_page.css?<?php echo time(); ?>">
    <script src="./public/js/FilmManipulation.js?<?php echo time(); ?>"></script>
    <script src="./public/js/App.js?<?php echo time(); ?>"></script>
    <script src="./public/js/main.js?<?php echo time(); ?>"></script>
</head>
<body>
<div id="parent_header">
    <div id="header" style="display: block;position: absolute;left: 2vw; ">
        <a href="/" id='front_head' style="text-align: center">front page</a>
    </div>
    <div style="margin-left: 17vw;">
        <div id="header" style="width: 50%">
            <span style="min-width: 100px">Find by:</span>
            <select id="sort_key">
                <option value="all" selected >None</option>
                <option value="film">Film Name</option>
                <option value="actor">Actor Name</option>
            </select>
            <input id="search_name" type="text" />
            <button id="find_by">OK</button>
        </div>
        <div>
            <a href="/?sort=1">Sort by name</a>
        </div>
        <div style="position: absolute; right: 10px; top: 40px">
            <button id="show_dialog">Add film</button>
        </div>
    </div>
</div>
<div id="search_block">
    <div id="close_window">X</div>
    <div class="add_actor">
        <div>
            <div class="add_actor">
                <div class="prop_film">Name: </div>
                <input id="film_name" type="text" />
            </div>
            <div class="add_actor">
                <div class="prop_film">Year: </div>
                <input id="film_year" type="text" />
            </div>
            <div class="add_actor">
                <div class="prop_film">Format: </div>
                <input id="film_format" type="text" />
            </div>
        </div>
        <div class="add_actor">
            <button id="add_actor" style="height: 21px">Add actor</button>
            <div id="actor_names_container">
            </div>
        </div>
    </div>
    <div style="display: flex;flex-direction: row;justify-content: center;">
        <button id="add_film" style="width: 200px;height: 50px">Add film</button>
    </div>

    <div>
        <span>Upload from file</span>
        <input id="file_input" type="file" />
    </div>
</div>
<div id="content_block">
    <?php
        echo $film_list;
    ?>
</div>
</body>
</html>