START TRANSACTION;

DROP TABLE IF EXISTS `WEBY_TEST`.`FILMS`;
CREATE TABLE `WEBY_TEST`.`FILMS` (
  ID        INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  NAME      VARCHAR(16) NOT NULL,
  DT_CREATE DATE NOT NULL,
  FORMAT    VARCHAR (20) NOT NULL
);

DROP TABLE IF EXISTS `WEBY_TEST`.`ACTORS`;
CREATE TABLE `WEBY_TEST`.`ACTORS` (
  ID    INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  NAME  VARCHAR (50)
);

DROP TABLE IF EXISTS `WEBY_TEST`.`FILMS_AND_ACTORS`;
CREATE TABLE `WEBY_TEST`.`FILMS_AND_ACTORS` (
  ID        INT NOT NULL AUTO_INCREMENT,
  FILM_ID   INT NOT NULL,
  ACTOR_ID  INT NOT NULL,

  PRIMARY KEY (ID),
  FOREIGN KEY (FILM_ID) REFERENCES `FILMS` (ID),
  FOREIGN KEY (ACTOR_ID) REFERENCES `ACTORS` (ID)
);

INSERT INTO `WEBY_TEST`.`FILMS` (NAME, DT_CREATE, FORMAT) VALUES
('KINO', '2018-01-01', 'VHS'),
('cHAHA', '2018-01-04', 'VHS'),
('MAKAK', '2018-01-02', 'VHS'),
('LENON', '2018-11-04', 'VHS'),
('MORTI', '2018-02-04', 'VHS'),
('KEK', '2018-01-01', 'VHS'),
('KINO', '2018-01-01', 'VHS'),
('cHAHA', '2018-01-04', 'VHS'),
('MAKAK', '2018-01-02', 'VHS'),
('LENON', '2018-11-04', 'VHS'),
('MORTI', '2018-02-04', 'VHS'),
('KEK', '2018-01-01', 'VHS'),
('KINO', '2018-01-01', 'VHS'),
('cHAHA', '2018-01-04', 'VHS'),
('MAKAK', '2018-01-02', 'VHS'),
('LENON', '2018-11-04', 'VHS'),
('MORTI', '2018-02-04', 'VHS'),
('KEK', '2018-01-01', 'VHS'),
('KINO', '2018-01-01', 'VHS'),
('cHAHA', '2018-01-04', 'VHS'),
('MAKAK', '2018-01-02', 'VHS'),
('LENON', '2018-11-04', 'VHS'),
('MORTI', '2018-02-04', 'VHS'),
('KEK', '2018-01-01', 'VHS');

INSERT INTO `WEBY_TEST`.`ACTORS` (NAME) VALUES
('NIKKI'),
('CHORT'),
('BANDERAS'),
('RIKARDO MILOS'),
('SASH GR'),
('MAKSYM STORCHAK');


INSERT INTO `WEBY_TEST`.`FILMS_AND_ACTORS` (FILM_ID, ACTOR_ID) VALUES
(1, 4),
(1, 1),
(2, 5),
(2, 4),
(4, 4),
(3, 2),
(3, 4),
(3, 1),
(5, 4),
(6, 4),
(6, 1),
(6, 2);

COMMIT;
