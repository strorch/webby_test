
## weby test

Test project to webby lab

It was created with Windows, so you need to configure your XAMPP stack.

To project start you need to:
1. Pull git repository
2. Add your MySQL database connection configuration to start.cmd.
Also need to add configurations to ./config/setup.php
3. You can start this app via `start.cmd`, but also you can by apache2(by using configuration file from ``./config``)
4. Start app

## Code description

``config`` folder: contains configuration files and sql migration scripts

``public`` folder: contains js, css and templates files

``erv`` folder:
1. `routes` dir contains requests handling
2. `app` dir contains main project part
