<?php

define('ROOTPATH', __DIR__);

require_once ROOTPATH."/config/database.php";
require_once ROOTPATH."/serv/app/Parent/customFunctions.php";
require_once ROOTPATH."/serv/app/Parent/Router.php";
require_once ROOTPATH."/serv/app/Parent/DB_connection.php";
require_once ROOTPATH."/serv/app/Views/ViewUtils.php";
require_once ROOTPATH."/serv/app/PageController.php";
require_once ROOTPATH."/serv/app/FilmApp.php";
require_once ROOTPATH."/serv/app/FilmView.php";

require_once ROOTPATH.'/serv/routes/web.php';

