<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 15.02.2019
 * Time: 22:37
 */

class FilmApp
{
    /*
$stmt = $pdo->prepare('SELECT * FROM employees WHERE name = :name');
$stmt->execute(array('name' => $name));
     */
    private $connection;

    public function __construct()
    {
        $this->connection = new DBConnection();
    }

    public function readFromFile($input_line)
    {
//        explode('Format: ', $input_line);
        $res = preg_split('/[\n\r].*Title: (.*)\s*([^\n\r]*)/', $input_line);
//        foreach ($res as $item) {
//            echo $item.'   AAAAAAAAAAAAAAAAAA';
//        }
        return $res;
    }

    public function CreateFilm($body)
    {
        $insertFilm = "
            INSERT INTO `WEBY_TEST`.`FILMS` 
                        (NAME, DT_CREATE, FORMAT) VALUES 
                        (:name, :date, :format);
        ";
        $params = [
            'name' =>html_entity_decode($body['film_name']),
            'date' => html_entity_decode($body['film_year']).'-01-01',
            'format' => html_entity_decode($body['film_format']),
        ];
        $this->connection->exec($insertFilm, $params);
        $film_id = $this->connection->DBH->lastInsertId();
        ////////////////////////////////////////////////////////////
        foreach ($body['actor_names'] as $actor)
        {
            $insertActor = "
            INSERT INTO `WEBY_TEST`.`ACTORS` 
                          (NAME) VALUES
                          (:name);
            ";
            $params = [
                'name' => html_entity_decode($actor)
            ];
            $this->connection->exec($insertActor, $params);
            $actor_id = $this->connection->DBH->lastInsertId();
            ////////////////////////////////////////////////////////////////////
            $insertActorFilm = "
            INSERT INTO `WEBY_TEST`.`FILMS_AND_ACTORS` 
                            (FILM_ID, ACTOR_ID) VALUES
                            (:film, :actor)
            ";
            $params = [
                'film' => intval($film_id),
                'actor' => intval($actor_id)
            ];
            $this->connection->exec($insertActorFilm, $params);
        }
        return $film_id;
    }
    public function DeleteFilm($body)
    {
        $insertFilm = "
            DELETE FROM `films_and_actors` WHERE FILM_ID=:id;
            DELETE FROM `films` WHERE id=:id
        ";
        $params = [
            'id' => (html_entity_decode($body['id']))
        ];
        $this->connection->exec($insertFilm, $params);
    }
    public function getFilmsArray($body)
    {
        $orderStr = "";
        if (isset($body['sort']) && $body['sort'] == 1) {
            $orderStr = 'order by t.name';
        }
        $queryString = "
            SELECT T.ID,
                   T.NAME,
                   T.DT_CREATE,
                   T.FORMAT
              FROM `films` T
        ".$orderStr;
        $res = $this->connection->query($queryString);
        return $res;
    }
    public function getFilmsArrayLike($body)
    {
        $queryString = "";
        if ($body['val'] === 'film')
            $queryString = "
                SELECT T.ID,
                       T.NAME,
                       T.DT_CREATE,
                       T.FORMAT
                  FROM `films` T
                 where T.NAME like ?
            ";
        elseif ($body['val'] === 'actor')
            $queryString = "
                SELECT distinct 
                       T1.ID,
                       T1.NAME,
                       T1.DT_CREATE,
                       T1.FORMAT
                  FROM `films`					t1
                  JOIN `films_and_actors`		t2	on t1.id = t2.film_id
                  JOIN `actors`					t3	on t2.actor_id = t3.id
                 where t3.name like ?
            ";
        if ($queryString === "")
            return null;
        $res = $this->connection->query($queryString, ['%'.$body['name'].'%']);
        return $res;
    }

    public function getFilmInfo($id)
    {
        $film_str = "      
            SELECT 
                T1.NAME,
                T1.DT_CREATE,
                T1.FORMAT
            FROM WEBY_TEST.FILMS 				T1
           WHERE t1.id=:id
        ";
        $actor_str = "
          SELECT DISTINCT
            	T3.ID,
                T3.NAME
            FROM WEBY_TEST.FILMS_AND_ACTORS		T2
            JOIN WEBY_TEST.ACTORS				T3	ON T2.ACTOR_ID=T3.ID
           WHERE T2.FILM_ID = :id
        ";
        $parameter = ['id' => $id];
        $film = $this->connection->query($film_str, $parameter );
        $actors = $this->connection->query($actor_str, $parameter );
        return [
            'film' => $film,
            'actors' => $actors
        ];
    }
}