<?php
class ViewUtils
{
    public static function getFilmListToDraw($list)
    {
        $str = "";
        foreach ($list as $key => $item)
        {
            $str .= "
                <div class='film_item' id='block_".$item["ID"]."'>
                    <div class='film_properties'>
                        <a href='/film_info?id=".$item["ID"]."'>
                            <p class='property'>Film name: ".$item["NAME"]."</p>
                        </a>
                        <p class='property'>Date create: ".$item["DT_CREATE"]."</p>
                        <p class='property'>Format: ".$item["FORMAT"]."</p>
                    </div>
                    <div>
                        <button value='".$item["ID"]."' class='delete_button'>Delete</button>
                    </div>
                </div>
            ";
        }
        return $str;
    }

    public static function getFilmInfo($item)
    {
        $film = $item['film'][0];
        $actors = $item['actors'];

        $tmp = "";
        foreach ($actors as $key => $actor)
        {
            if ($key === count($actors) - 1)
                $tmp .= "<span>".$actor['NAME']."</span> ";
            else
                $tmp .= "<span>".$actor['NAME'].",</span> ";
        }

        $str = "
            <div class='film_item'>
                <div class='film_properties'>
                    <p class='property'>Film name: ".$film["NAME"]."</p>
                    <p class='property'>Date create: ".$film["DT_CREATE"]."</p>
                    <p class='property'>Format: ".$film["FORMAT"]."</p>
                    <p class='property'>Actors: ".$tmp."</p>
                </div>
            </div>
        ";
        return $str;
    }
}