<?php

class FilmView
{
    public function __construct(){}

    private function getList($film_arr)
    {
        return ViewUtils::getFilmListToDraw($film_arr);
    }
    public function showFilmList($film_arr)
    {
        $film_list = $this->getList($film_arr);

        require_once './public/blades/front_page.php';
    }
    public function getFilmList($film_arr)
    {
        return $this->getList($film_arr);
    }

    public function getFilmInfo($film_info)
    {
        $film_list = ViewUtils::getFilmInfo($film_info);
        require_once './public/blades/filmInfo.php';
    }

    public function get404()
    {
        require_once './public/blades/404.php';
    }
}