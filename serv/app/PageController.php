<?php

class PageController
{
    private $listModel;
    private $listView;

    public function __construct()
    {
        $this->listModel = new FilmApp();
        $this->listView = new FilmView();
    }

    public function FrontPage()
    {
        $film_arr = $this->listModel->getFilmsArray($_GET);
        $this->listView->showFilmList($film_arr);
    }
    public function FindBy()
    {
        if (!isset($_GET['name']) || !isset($_GET['val']))
            echo json_encode('error');

        $film_arr = $this->listModel->getFilmsArrayLike($_GET);
        if ($film_arr === null)
            echo json_encode('error');

        $view = $this->listView->getFilmList($film_arr);
        echo json_encode($view);
    }

    public function FilmInfo()
    {
        $film_arr = $this->listModel->getFilmInfo($_GET['id']);
//        echo json_encode($film_arr);
        $this->listView->getFilmInfo($film_arr);
    }

    public function DelFilm()
    {
        $body = fetchParse();

        if (!isset($body['id']))
        {
            echo json_encode([
                'success' => true,
                'message' => "wrong input"
            ]);
        }
        $this->listModel->DeleteFilm($body);
        echo json_encode($body);
    }

    public function AddFilm()
    {
        $body = fetchParse();
        if (!isset($body['film_format']) ||
            !isset($body['film_name']) ||
            !isset($body['film_year']) ||
            empty($body['actor_names']))
        {
            echo json_encode([
                'success' => true,
                'message' => "wrong input"
            ]);
        }

        $id = $this->listModel->CreateFilm($body);

        $child = ViewUtils::getFilmListToDraw([[
            "ID" => $id,
            "NAME" => $body['film_name'],
            "DT_CREATE" => $body['film_year'],
            "FORMAT" => $body['film_format']
        ]]);
        echo json_encode([
            'success' => true,
            'new_film' => $child
        ]);
    }

    public function LoadFile()
    {
        $content = file_get_contents($_FILES['files']['tmp_name']);
        $splitted = $this->listModel->readFromFile($content);
        echo json_encode($splitted);
//        dd( json_encode($_FILES['files']));
    }

    public function ErrorPage()
    {
        $this->listView->get404();
    }
}