<?php

class DBConnection
{
    public $DBH;

    public function __construct()
    {
        $params = $this->getConnectionParams();
        $res = $this->getPDOConnection($params);
        if ($res['status'] === false) {
            echo $res['message'];
            die;
        }
    }

    public  function query($command, $params = null)
    {
        if (is_null($params))
        {
            $res = $this->DBH->query($command);
            return $res->fetchAll();
        }
        $prepared = $this->DBH->prepare($command);
        $prepared->execute($params);
        return $prepared->fetchAll();
    }

    public  function exec($command, $params = null)
    {
        if (is_null($params))
        {
            $this->DBH->exec($command);
        }
        $prepared = $this->DBH->prepare($command);
        $prepared->execute($params);
    }
    private function getPDOConnection($params)
    {
        try
        {
            $this->DBH = new PDO($params['DSN'], $params['user'], $params['passwd']);
            return [
                'status'=> true
            ];
        }
        catch (PDOException $ex)
        {
            $ex->getMessage();
            return [
                'status'=> false,
                'message' => $ex->getMessage()
            ];
        }
    }

    private function getConnectionParams()
    {
        require ROOTPATH."/config/database.php";

        return [
            "DSN" => "mysql:host=$DB_HOST;dbname=$DB_NAME;port=$DB_PORT" ,
            "user" => $DB_USER,
            "passwd" => $DB_PASSWORD
        ];
    }
}