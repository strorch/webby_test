<?php

Router::get("/", 'PageController@FrontPage');

Router::post("/del_film", 'PageController@DelFilm');
Router::post("/add_film", 'PageController@AddFilm');
Router::post("/load_file", 'PageController@LoadFile');

Router::get("/film_info", 'PageController@FilmInfo');

Router::get("/find_by", 'PageController@FindBy');

Router::error_page();